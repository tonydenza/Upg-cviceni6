﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni6._1
{
    class Program
    {

        static int RuznychPismen (string s)
        {
            int pocet = 0;
            int velikost = 'z' - 'a' + 1;
            string retezec = s.ToLower();
            bool[] pismena = new bool[velikost];

            for (int i = 0; i < retezec.Length; i++)
                if (char.IsLetter(retezec[i]))
                    pismena[(int)retezec[i]-'a'] = true;

            for (int j = 0; j < pismena.Length; j++)
                if (pismena[j])
                    pocet++;

            return pocet;
        }

        static int PocetSlov (string s) //posledni slovo check
        {
            int pocet = 0;

            for (int i = 1; i < s.Length; i++)
                if ( ! char.IsLetter(s[i]) && char.IsLetter(s[i-1]))
                    pocet++;

            return pocet;
        }

        static string PrvniSlovo(string retezec, int pozice)
        {
            int start = 0, konec = 0;

            if (pozice > retezec.Length - 1 || pozice < 0)
                return "";

            for (int i = pozice; i < retezec.Length; i++)
            {
                if (!char.IsLetter(retezec[i]))
                    continue;

                if (char.IsLetter(retezec[i]) && i > pozice)
                {
                    start = i;
                    konec = i;
                    while (char.IsLetter(retezec[konec]) && konec <= retezec.Length)
                        konec++;
                    break;
                }
                else if (char.IsLetter(retezec[i]) && i == pozice)
                {
                    start = i;
                    konec = i;
                    while (char.IsLetter(retezec[konec + 1]) && konec < retezec.Length)
                        konec++;
                    konec++;
                    while (char.IsLetter(retezec[start]) && start > 0)
                        start--;
                    if (!char.IsLetter(retezec[start]))
                        start++;
                    break;
                }
            }
            return retezec.Substring(start, konec-start);
        }

        static string NejdelsiSlovo(string retezec)
        {
            int start = 0, konec = 0, max = 0;
            int s = 0, k = 0;

            for (int i = 0; i < retezec.Length; i++)
            {
                if (char.IsLetter(retezec[i])) {
                    start = i;
                    konec = i;
                    while (char.IsLetter(retezec[konec+1]) && konec < retezec.Length)
                        konec++;
                    konec++;
                    if (konec - start > max)
                    {
                        max = konec - start;
                        s = start;
                        k = konec;
                    }
                }
            }
            return retezec.Substring(s, k- s);
        }

        static void Main(string[] args)
        {
            string s1 = "Alena ma 2 ruce.";
            int ruznych = RuznychPismen(s1);
            int pocet = PocetSlov(s1);
            string s2 = PrvniSlovo(s1, 2);
            string s3 = PrvniSlovo(s1, s1.Length - 1);
            string s4 = NejdelsiSlovo(s1);
            //string s5 = NejvetsiSlovo(s1); // "ruce"
            Console.ReadLine();
        }
    }
}
