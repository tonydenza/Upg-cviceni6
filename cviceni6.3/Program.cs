﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni6._3
{
    class Program
    {
        static int Compare(char[] s1, char[] s2)
        {
            int cyklus = 0;

            if (s1.Length == 0)
                return 1;
            else if (s2.Length == 0)
                return -1;

            if (s1.Length > s2.Length)
                cyklus = s2.Length;
            else
                cyklus = s1.Length;

            for (int i = 0; i < cyklus; i++)
            {
                    if (s1[i] < s2[i] || i == cyklus-1 && s2.Length > s1.Length)
                        return -1;
                    else if (s1[i] > s2[i] || i == cyklus - 1 && s1.Length > s2.Length)
                        return 1;
                    else
                        continue; 
            }
            return 0;
        }

        static int IndexOf(char[] retezec, char[] hledany, int index)
        {
            int pozice = 0, pocatek = 0;

            if (retezec.Length == 0 || hledany.Length == 0)
                return -1;

            for (int i = index; i < retezec.Length; i++)
            {
                while (retezec[i] == hledany[pozice])
                {
                    if (pozice == 0)
                        pocatek = i;

                    if (pozice + 1 == hledany.Length)
                        return pocatek;
                    pozice++;
                }
            }

            return -1;
        }

        static char[] Substring (char[] retezec, int start, int delka)
        {
            char[] sub;
            int j = 0;

            if (start + delka > retezec.Length || retezec.Length == 0)
                return sub = "".ToCharArray();
            else
                sub = new char[delka];

            for (int i = start; i < start + delka; i++, j++)
                sub[j] = retezec[i];

            return sub;
        }

        static char[] Insert (char[] retezec, int start, char[] vkladany)
        {
            char[] cele;
            int j = 0;

            if (start > retezec.Length)
                return cele = "".ToCharArray();
            else
                cele = new char[retezec.Length + vkladany.Length];

            for (int i = 0; i < start; i++)
                cele[i] = retezec[i];

            for (int i = start; i < cele.Length; i++)
            {
                if (j < vkladany.Length)
                {
                    cele[i] = vkladany[j];
                    j++;
                    continue;
                }
                else
                {
                    if (start == retezec.Length)
                        continue;
                    cele[i] = retezec[start];
                    start++;
                }
            }

            return cele;
        }

        static void Main(string[] args)
        {
            char[] s1 = "klokan".ToCharArray();
            char[] s2 = "klobouk".ToCharArray();
            int i1 = Compare(s1, s2); // a)
            int i2 = IndexOf(s2, "o".ToCharArray(), 3);  // b)
            char[] s3 = Substring(s2, 2, 3); // c)
            char[] s4 = Insert(s1, 1, s3); // d)
            /*
            char[] s5 = Remove(s2, 3, 2); // 'k''l''o''u''k''
            char[] s6 = Replace(s5, "k".ToCharArray(), s3);
            // 'o''b''o''l''o''u''o''b''o'
            */
            Console.Read();
        }
    }
}
