﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cviceni6._2
{
    class Program
    {
        static string CezarovaSifra(string zprava, int posun)
        {
            char[] retezec = new char[zprava.Length];
            int cislo;

            retezec = zprava.ToCharArray();

            for (int i = 0; i < retezec.Length; i++)
            {
                cislo = (int)retezec[i] + posun;
                retezec[i] = (char)cislo;
            }

            zprava = new string(retezec);
            return zprava;
        }

        static void Main(string[] args)
        {
            string s = "Toto je tajny text: 1726";
            string zasifrovane = CezarovaSifra(s, 20);
            // h4~y4u~4yN4EKFJ
            string odsifrovane = CezarovaSifra(zasifrovane, -20);
            // Musíme získat původní text

            /* odpoved na b)
             * 
             * Vyzkousim vsechny posuny do cisla 255 (velikost ASCII tabulky).
             * Pro delsi texty bych vysledny text mohl porovnavat s cetnosti znaku napr. anglicke abecedy.

            for (int i = 0; i < 256; i++)
            {
            string utocnik = CezarovaSifra(zasifrovane, -i);
            Console.WriteLine("{0}: {1}", i, utocnik);
            }
            */
            Console.Read();

}
}
}
